# Case de Gerenciamento de Base de Dados

Será mostrado um projeto que usa o conceito de migrações de banco de dados como código para auxiliar no gerenciamento das mudanças do banco de dados ao longo do projeto integrado com o processo de integração contínua.

Algumas ferramentas que auxiliam nesse processo:

- Liquibase [https://www.liquibase.org]
- MyBatis Migrations [http://www.mybatis.org/migrations/]
- Active Record Migrations [https://api.rubyonrails.org/classes/ActiveRecord/Migration.html]
- Flyway [https://flywaydb.org]
